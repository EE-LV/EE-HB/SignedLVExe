Readme
======

Refer to https://knowledge.ni.com/KnowledgeArticleDetails?id=kA00Z0000004Ay9SAE for details on digitally signing an application or installer in LabVIEW Application Builder.

Author: H.Brand@gsi.de

Copyright 2018  GSI Helmholtzzentrum f�r Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europ�ischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie d�rfen dieses Werk ausschlie�lich gem�� dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEW�HRLEISTUNG ODER BEDINGUNGEN - ausdr�cklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschr�nkungen unter der Lizenz sind dem Lizenztext zu entnehmen.